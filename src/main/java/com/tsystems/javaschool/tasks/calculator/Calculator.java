package com.tsystems.javaschool.tasks.calculator;


import java.util.*;

public class Calculator {

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    public String evaluate(String statement) {
        double result;
        try {
            result= Double.valueOf(calculatePolishNotation(reversPolishNotation(statement)));
        }catch (Exception e){
            return null;
        }

        if(result==Double.POSITIVE_INFINITY)return null;
        if(result%1.0==0) {
            return String.valueOf((int) result);
        }
        return String.valueOf(result);
    }


    /**
     * @param statement expression
     * @return  expression in polish notation form
     */
    private String reversPolishNotation(String statement){

        Map<String,Integer> operators=new HashMap<>();
        operators.put("+",2);
        operators.put("-",2);
        operators.put("*",1);
        operators.put("/",1);

        List<String> result=new LinkedList<>();
        Stack<String> stack=new Stack<>();

        statement=statement.replace(" ","");

        Set<String> operationSymbols=new HashSet<>(operators.keySet());
        operationSymbols.add("(");
        operationSymbols.add(")");

        int x = 0;

        boolean findNext = true;
        while (findNext) {
            int nextOperationIndex = statement.length();
            String nextOperation = "";

            for (String operation : operationSymbols) {
                int i = statement.indexOf(operation, x);
                if (i >= 0 && i < nextOperationIndex) {
                    nextOperation = operation;
                    nextOperationIndex = i;
                }
            }

            if (nextOperationIndex == statement.length()) {
                findNext = false;
            } else {

                if (x != nextOperationIndex) {
                    result.add(statement.substring(x, nextOperationIndex));
                }
                if (nextOperation.equals("(")) {
                    stack.push(nextOperation);
                }

                else if (nextOperation.equals(")")) {
                    while (!stack.peek().equals("(")) {
                        result.add(stack.pop());
                        if (stack.empty()) {
                            return null;
                        }
                    }
                    stack.pop();
                }

                else {
                    while (!stack.empty() && !stack.peek().equals("(") &&
                            (operators.get(nextOperation) >= operators.get(stack.peek()))) {
                        result.add(stack.pop());
                    }
                    stack.push(nextOperation);
                }
                x = nextOperationIndex + nextOperation.length();
            }
        }

        if (x != statement.length()) {
            result.add(statement.substring(x));
        }

        while (!stack.empty()) {
            result.add(stack.pop());
        }
        StringBuilder res = new StringBuilder();
        if (!result.isEmpty())
            res.append(result.remove(0));
        while (!result.isEmpty())
            res.append(" ").append(result.remove(0));

        return res.toString();
    }


    /**
     * @param statement is polish notation expression
     * @return answer of expression
     */
    private String calculatePolishNotation(String statement){

        Stack<String> res= new Stack<>();
        String[] arr=statement.split(" ");
        for(String a:arr){
            if(Objects.equals(a, "+") || Objects.equals(a, "-") || Objects.equals(a, "*") || Objects.equals(a, "/")){
                if(Objects.equals(a, "+")){
                    res.push(String.valueOf(Double.valueOf(res.pop())+Double.valueOf(res.pop())));
                }
                if(Objects.equals(a, "-")){
                    Double x= Double.valueOf(res.pop());
                    res.push(String.valueOf(Double.valueOf(res.pop())-x));
                }
                if(Objects.equals(a, "*")){
                    res.push(String.valueOf(Double.valueOf(res.pop())*Double.valueOf(res.pop())));
                }
                if(Objects.equals(a, "/")){
                    Double x=Double.valueOf(res.pop());
                    res.push(String.valueOf(Double.valueOf(res.pop())/x));
                }
            }else {
                res.push(a);
            }
        }
        if(res.size()!=1)return null;
        return res.pop();
    }



}
