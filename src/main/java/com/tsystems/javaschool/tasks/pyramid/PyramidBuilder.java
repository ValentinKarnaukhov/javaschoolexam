package com.tsystems.javaschool.tasks.pyramid;

import java.util.*;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) {

        double height=(Math.sqrt(8*inputNumbers.size()+1)-1)/2;

        if(height%1.0!=0||inputNumbers.contains(null))throw new CannotBuildPyramidException();

        Collections.sort(inputNumbers);

        int[][] result=new int[(int) height][(int) (height*2-1)];
        int count=0;
        for(int i=0;i<height;i++){
            int margin= (int) (height-i-1);
            for(int j=margin;j<margin+(i+1)*2-1;j+=2){
                result[i][j]=inputNumbers.get(count++);
            }
        }
        return result;
    }


}
